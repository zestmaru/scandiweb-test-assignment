<?php
include_once("Db.php");
include_once("Items.php");
?>

<html lang="en">

<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Product List</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
            integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
            crossorigin="anonymous"></script>
</head>
<body>


<div class="container">
    <header class="py-3">
        <div class="text-left">
            <h2 class="float-left">Product List</h2>
        </div>
        <div class="text-right">
            <a href="add.php" class="btn btn-primary" role="button">Add</a>
            <input type="submit" name="deleteItems" value="Mass delete" class="btn btn-danger" form="deleteItems" disabled/>
        </div>
        <div class="dropdown-divider"></div>
    </header>
</div>

<div class="container">
    <form id="deleteItems" action="Delete.php" method="post">
        <div class="row">
            <?php
            $data = new Items();
            $data->showItems(); ?>
        </div>
        <script type="text/javascript">
            $(function(){
                $("input[type='checkBox']").change(function(){
                    var check = $("input[type='checkBox']:checked").length;
                    if(check == 0)
                        $("input[type='submit']").prop("disabled", true);
                    else
                        $("input[type='submit']").removeAttr("disabled");
                });
                $("input[type='checkBox']").trigger('change');
            });
        </script>
    </form>
</div>

</body>
</html>
