<?php

include('Db.php');

class Delete extends Db
{
    public function deleteItems()
    {
        if (!empty($_POST['check_list'])) {
            foreach ($_POST['check_list'] as $item) {
                $sql = 'delete from items where sku="'.$item.'" ';

                if ($this->connect()->query($sql)) {
                    header('Location: list.php');;
                } else {
                    echo "ERROR: Could not execute $sql. " . mysqli_error($this->connect());
                    return false;
                }
            }
        }
    }
}

if (isset($_POST['deleteItems'])) {
    $delete = new Delete();
    $delete->deleteItems();
}
