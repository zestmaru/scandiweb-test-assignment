create table items
(
    sku   varchar(12)               not null,
    name  varchar(255)              not null,
    price float(10, 2) default 0.00 not null,
    type  varchar(20)               not null,
    attr  varchar(255)              not null,
    constraint items_sku_uindex
        unique (sku)
);

