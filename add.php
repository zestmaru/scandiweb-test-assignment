<?php

?>

<html lang="en">

<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Product Add</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
            integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
            crossorigin="anonymous"></script>

    <script type="text/javascript">
        function validate() {
            if(isNaN(document.addForm.price.value)) {
                alert( "Price should contain only numbers and/or be separated with dot!" );
                document.addForm.price.focus() ;
                return false;
            }

            if(document.addForm.type.value === "dvd") {
                if(isNaN( document.addForm.size.value)) {
                    alert( "Size field should contain only numbers!" );
                    document.addForm.size.focus() ;
                    return false;
                }
            } else if (document.addForm.type.value === "book") {
                if(isNaN( document.addForm.weight.value)) {
                    alert( "Weight field should contain only numbers!" );
                    document.addForm.weight.focus() ;
                    return false;
                }
            } else if (document.addForm.type.value === "furniture") {
                if(isNaN( document.addForm.height.value)) {
                    alert( "Height field should contain only numbers!" );
                    document.addForm.height.focus() ;
                    return false;
                } else if(isNaN( document.addForm.width.value)) {
                    alert( "Width field should contain only numbers!" );
                    document.addForm.width.focus() ;
                    return false;
                } else if(isNaN( document.addForm.length.value)) {
                    alert( "Length field should contain only numbers!" );
                    document.addForm.length.focus() ;
                    return false;
                }
            }
            return true;
        }
    </script>

</head>
<body>

<div class="container">
    <header class="py-3">
        <div class="text-left">
            <h2 class="float-left">Product Add</h2>
        </div>
        <div class="text-right">
            <input type="submit" name="submit" id="formSubmit" value="Save" class="btn btn-success"
                   form="productAdd" onclick="return(validate())" disabled/>
            <a href="list.php" class="btn btn-danger" role="button">Cancel</a>
        </div>
        <div class="dropdown-divider"></div>
    </header>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <form id="productAdd" name="addForm" action="Form.php" method="post">
                <p>
                    <label for="sku">SKU:</label>
                    <input type="text" class="form-control" placeholder="" name="sku" id="sku" required>
                </p>
                <p>
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="" name="name" id="name" required>
                </p>
                <p>
                    <label for="price">Price ($):</label>
                    <input type="text" class="form-control" placeholder="" name="price" id="price" required>
                </p>
                <p>
                    <label for="type">Product Type:</label>
                    <select class="form-control" onchange="" name="type" id="switch" required>
                        <option selected>Select</option>
                        <option value="dvd">DVD</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </p>
                <script type="text/javascript">
                    $("#switch").change(function () {
                        var value = $(this).val();
                        switch (value) {
                            case "dvd":
                                document.getElementById('formSubmit').disabled = false;
                                $('form').append('<p class="size">Please, provide size<br><br>' +
                                    '<label for="size">Size (MB):</label>' +
                                    '<input type="text" class="form-control" placeholder="" name="size" id="size" required>' +
                                    '</p>');
                                $(".weight").remove();
                                $(".dimensions").remove();
                                break;
                            case "book":
                                document.getElementById('formSubmit').disabled = false;
                                $('form').append('<p class="weight">Please, provide weight<br><br>' +
                                    '<label for="weight">Weight (KG):</label>' +
                                    '<input type="text" class="form-control" placeholder="" name="weight" id="weight" required>' +
                                    '</p>');
                                $(".size").remove();
                                $(".dimensions").remove();
                                break;
                            case "furniture":
                                document.getElementById('formSubmit').disabled = false;
                                $('form').append('<p class="dimensions">Please, provide dimensions<br><br>' +
                                    '<label for="height">Height (CM):</label>' +
                                    '<input type="text" class="form-control" placeholder="" name="height" id="height" required>' +
                                    '</p>');
                                $('form').append('<p class="dimensions">' +
                                    '<label for="width">Width (CM):</label>' +
                                    '<input type="text" class="form-control" placeholder="" name="width" id="width" required>' +
                                    '</p>');
                                $('form').append('<p class="dimensions">' +
                                    '<label for="length">Length (CM):</label>' +
                                    '<input type="text" class="form-control" placeholder="" name="length" id="length" required>' +
                                    '</p>');
                                $(".size").remove();
                                $(".weight").remove();
                                break;
                            default:
                                document.getElementById('formSubmit').disabled = true;
                                $(".size").remove();
                                $(".weight").remove();
                                $(".dimensions").remove();
                        }
                    });
                </script>
            </form>
        </div>
    </div>
</div>

</body>
</html>
