<?php

include('Db.php');

class Form extends db
{

    public function getFormInput() {

        $sku = mysqli_real_escape_string($this->connect(), $_REQUEST['sku']);
        $name = mysqli_real_escape_string($this->connect(), $_REQUEST['name']);
        $price = str_replace(',', '.', mysqli_real_escape_string($this->connect(), $_REQUEST['price']));

        $type = mysqli_real_escape_string($this->connect(), $_REQUEST['type']);

        switch ($type) {
            case 'dvd':
                $attr = mysqli_real_escape_string($this->connect(), $_REQUEST['size']);
                break;
            case 'book':
                $attr = mysqli_real_escape_string($this->connect(), $_REQUEST['weight']);
                break;
            case 'furniture':
                $attr = mysqli_real_escape_string($this->connect(), $_REQUEST['height'])
                    . "x" .
                    mysqli_real_escape_string($this->connect(), $_REQUEST['width'])
                    . "x" .
                    mysqli_real_escape_string($this->connect(), $_REQUEST['length']);
                break;
        }

        $sql = "insert into items (sku, name, price, type, attr) 
                values ('$sku', '$name', '$price', '$type', '$attr')";

        if ($this->connect()->query($sql)) {
            header('Location: list.php');
        } else {
            echo "ERROR: Could not execute $sql. " . mysqli_error($this->connect());
            return false;
        }

    }

}

if (isset($_POST['submit'])) {
    $form = new Form();
    $form->getFormInput();
}