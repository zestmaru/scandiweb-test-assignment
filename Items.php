<?php

class Items extends Db
{

    protected function getItems()
    {
        $sql = "select * from items";
        $result = $this->connect()->query($sql);
        $numRows = $result->num_rows;

        if ($numRows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function showItems()
    {
        $items = $this->getItems();

        if (!empty($items)) {
            foreach ($items as $i) {
                echo '<div class="col-lg-3 col-md-6 col-sm-12">';
                echo '<div class="card" style="width: 200px; margin-bottom: 15px;">';
                echo '<input type="checkbox" id="' . $i['sku'] . '" name="check_list[]" value="' . $i['sku'] . '">';
                echo '<div class="card-block text-center">';
                echo '<p class="card-text">' . $i['sku'] . '</p>';
                echo '<p class="card-text">' . $i['name'] . '</p>';
                echo '<p class="card-text">' . $i['price'] . ' $</p>';
                switch ($i['type']) {
                    case 'dvd':
                        echo '<p class="card-text">' . 'Size: ' . $i['attr'] . ' MB</p>';
                        break;
                    case 'book':
                        echo '<p class="card-text">' . 'Weight: ' . $i['attr'] . 'KG</p>';
                        break;
                    case 'furniture':
                        echo '<p class="card-text">' . 'Dimensions: ' . $i['attr'] . '</p>';
                        break;
                }
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
        } else {
            echo '<p> No items in database! Add something by pressing add button!</p>';
        }
        return $items;
    }
}